import styled from 'styled-components';
import { IForm } from '..';

export const FormStyled = styled.form<IForm>`
  background: ${({ size }) => (size === 'medium' ? '#9CC6D8' : '#000')};
  border-radius: 4px;
  padding: 20px;
  color: #fff;
  display: flex;
  flex-direction: column;
  max-width: ${({ size }) => (size === 'large' ? '500px' : '700px')};
  width: 100%;
  gap: 20px;

  > input {
    height: 30px;
  }

  > span {
    color: #e34646;
    text-align: center;
  }

  > button {
    background: #09f;
    height: 50px;

    &:disabled {
      background-color: #a0a0a0;
      cursor: not-allowed;
    }
  }

  @media screen and (max-width: 768px) {
    background: red;
  }
`;
