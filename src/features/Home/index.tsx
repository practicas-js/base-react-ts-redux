import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import Button from '../../components/Button';
import Modal from '../../components/Modal';
import { fetchItemsAsync, getItems, getItemsStatus } from '../../redux/cartSlice';
import { Cart } from './components/Cart';
import Form from './components/Form';
import { Items } from './components/Items';
import { HomeStyled, HomeWrapperStyled } from './styles';

const Home = () => {
  const dispatch = useAppDispatch();
  const itemList = useAppSelector(getItems);

  const [isOpentModal, setIsOpentModal] = useState(false);

  useEffect(() => {
    dispatch(fetchItemsAsync());
  }, []);


  return (
    <>
      <HomeWrapperStyled>
        <h1>Tienda</h1>
        <Button value="Crear ítem" onClick={() => setIsOpentModal(true)} />
        <HomeStyled>
          <Items list={itemList} />
          <Cart data-testid="cart-component" />
        </HomeStyled>
        <Modal isOpent={isOpentModal} setIsOpent={setIsOpentModal}>
          <Form onClose={setIsOpentModal} />
        </Modal>
      </HomeWrapperStyled>
    </>
  );
};

export default Home;
